#include <wiringPI.h>
#include <iostream>


const int Tuerzustand1=0;
const int Tuerzustand2=1;
const int Klingel=8;
const int Tuerschliessen=9;
const int Tueroffner=10;

int checkklingel(){
    pinMode(klingel,Input);
    if (digitalread(Klingel)==1){
        return 1;
    }
    return 0;
}

int checkTuerschliessen(){
    pinMode(Tuerschliessen,Input);
    if (digitalread(Tuerschliessen)==1){
        return 1;
    }
    return 0;
}

int checkTueroffner(){
    pinMode(Tueroffner,Input);
    if (digitalread(Tueroffner)==1){
        return 1;
    }
    return 0;
}


void changeTuerzustand1(){
    pinMode(Tuerzustand1,Output);
    pinMode(Tuerzustand2,Output);
    digitalwrite (Tuerzustand1,1);
    digitalwrite (Tuerzustand2,0);
}

void changeTuerzustand2(){
    pinMode(Tuerzustand1,Output);
    pinMode(Tuerzustand2,Output);
    digitalwrite (Tuerzustand1,0);
    digitalwrite (Tuerzustand2,1);
}



/*int main(){
if(wiringPiSetup==-1){
    return 1;
}
pinMode(PinLed,Output);
pinMode(PinButton,Input);
while(1){
    digitalwrite(PinLed,!digitalread(PinButton));
}

}

