/*
	Dateiname: main.cpp
	Autor: Fabian Müller
	Änderungsdatum: 18.01.2018

	Dieses Programm steuert die gesamte Anlage der Türklingel.
	Es fragt den aktuellen Systemzustand (aus [0], automatisch [1] oder manuell [2] ab,
	gibt ihn aus und ermöglicht Besuchern, die Klingel zu betätigen.
*/

#include "Besucher.h"
#include "Tuersystem.h"
#include "Weboberflaeche.h"
#include "Benutzer.h"
#include "email.cpp"
#include "sound.cpp"
#include "wire1.cpp"

int main()
{
    mysqlpp::Connection datenbank(false); // Anlegen einer Verbindung zur Datenbank für Kommunikation mit der Weboberfläche
    if(!datenbank.connect("Tuerklingel","localhost","root","123456")) // Hersetellen der Verbindung und Abfrage, ob diese fehlgeschlagen ist
    {
        cout << "Die Verbindung zur Datenbank ist fehlgeschlagen." << endl; // Fehlermeldung
        cout << "Das Programm wird nun beendet." << endl; // Fehlermeldung
        return -1; // Beendigung des Programms aufgrund des Fehlers
    }
    Besucher besucher; // Instanz der Klasse Besucher anlegen
    bool klingel = 0;
    bool tuerschliessen = 0;
    bool tuerzustand1 = 0; //tür offen
    bool tuerzustand1 = 0; //tür geschlossen
    bool tueroffner = 0;
    int systemzustand = 0;
    int zahler = 0;

    int breaker = 0;

    while(1) // Start der Endlosschleife für Dauerbetrieb
    {
         // Simulation des Schalters für die Klingel
        //cout << "Geben Sie [1] ein, um zu klingeln." << endl; // Abfrage, ob geklingelt werden soll
        //cout << "Ansonsten wird das Programm beendet." << endl;
        //cin >> klingel; // Eingabe: Klingeln [1]; Beenden [0]
        //Weboberflaeche::setZustand(Weboberflaeche::zustandAuslesen(datenbank)); // Auslesen des Systemzustands aus der Konfigurationsdatei
        //cout << "Zustand: " << Weboberflaeche::getZustand() << endl; // Ausgabe des Systemzustands
        //Abfragen der Zustände
        int zahler1 = 0;
        klingel = checkklingel(); // =1 wenn geklingelt wurde
        tuerschliessen = checkTuerschliessen(); //=1 wenn Taster gedrückt wurde
        Weboberflaeche::setZustand(Weboberflaeche::zustandAuslesen(datenbank));
        systemzustand = Weboberflaeche::getZustand(); //Auslesen des Systemzustandes
        if (zahler == 0){
            breaker = 0;
        }

        if (klingel==1 && breaker == 0){
            breaker = 1;
            sendmail();
            besucher.klingeln(datenbank);
            if (systemzustand == 0){ //wenn Systemzustand auf automatisch, dann Tür öffnen
                changeTuerzustand1();
                sound(); //und Sound abspielen
            }
            if (systemzustand == 1){ //wenn Systemzustand auf manuell
                while (zahler1<3000000){ //30 sekunden auf Türöffnung warten
                    usleep(1);
                    zahler1 = zahler1 +1;
                    tueroffner = checkTueroffner();
                    if (tueroffner==1){
                        changeTuerzustand1();
                        sound();
                    }
                }

            }
        }
        if (tuerschliessen == 1){ //wenn tuerschließer gedrückt wurde, dann tuer schließen
            changeTuerzustand2();
        }








        zahler = zahler + 1;
        //Nur einmal pro Sekunde Klingeleingabe entgegennehmen
        if (zahler > 10000){
            zahler = 0;
            breaker = 0;
        }
        usleep (10);
    }

    return 0;

}
