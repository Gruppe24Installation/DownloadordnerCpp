#include <wiringPI.h>
#include <iostream>


const int PinLed=0;
const int PinButton=8;



int main(){
if(wiringPiSetup==-1){
    return 1;
}
pinMode(PinLed,Output);
pinMode(PinButton,Input);
while(1){
    digitalwrite(PinLed,!digitalread(PinButton));
}

}
